from fastai.vision import *


def PlanePredict(image):

    learn = load_learner(Path(__file__).parent, "export.pkl")
    return str(learn.predict(image)[0])
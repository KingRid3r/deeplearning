import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeComponent} from './home.component';
import {RouterModule, Routes} from '@angular/router';
import {ExternalModulesModule} from '../../external-modules/external-modules.module';

const routes: Routes = [
    {
      path: '',
      component: HomeComponent,
    }
  ];

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    ExternalModulesModule,
    RouterModule.forChild(routes)
  ]
})
export class HomePageModule { }

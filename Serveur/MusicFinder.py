import librosa
from fastai.vision import *




def MusicPredict(song):
    learn = load_learner(Path(__file__).parent, "export1.pkl")
    return str(learn.predict(song)[0])

def ConvertToImage(song):
    x, sample_rate = librosa.load(song, offset=30, duration=30)
    n_fft = 1024
    hop_length = 256
    n_mels = 40
    fmin = 20
    fmax = sample_rate / 2

    mel_spec_power = librosa.feature.melspectrogram(x, sr=sample_rate, n_fft=n_fft,
                                                    hop_length=hop_length,
                                                    n_mels=n_mels, power=2.0,
                                                    fmin=fmin, fmax=fmax)
    mel_spec_db = librosa.power_to_db(mel_spec_power, ref=np.max)
    plt.imsave(os.path.abspath('imgToPredict.png'), mel_spec_db)



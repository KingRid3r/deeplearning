import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public url = environment.url;
  public selectedPlaneFile: File;
  public selectedSongFile: File;
  public lastPlaneDetection: string;
  public lastSongDetection: string;
  public songloading = false;
  public planeloading = false;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  onPlaneFileChanged(event): void {
    event.preventDefault();
    this.selectedPlaneFile = event.target.files[0];
  }

  onSongFileChanged(event): void {
    event.preventDefault();
    this.selectedSongFile = event.target.files[0];
  }


  onPlaneUpload($event): void {
    $event.preventDefault();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedPlaneFile, this.selectedPlaneFile.name);
    this.planeloading = true;
    this.http.post(this.url + '/plane-upload', uploadData, {responseType: 'text'})
      .subscribe((res) => {
        console.log(res);
        this.lastPlaneDetection = res as string;
        this.planeloading = false;

      });
  }

  onSongUpload($event): void {
    $event.preventDefault();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedSongFile, this.selectedSongFile.name);
    this.songloading = true;

    this.http.post(this.url + '/song-upload', uploadData, {responseType: 'text'})
      .subscribe((res) => {
        console.log(res);
        this.lastSongDetection = res as string;
        this.songloading = false;
      });
  }

  formPrevent($event: MouseEvent): void {
    $event.preventDefault();
  }
}

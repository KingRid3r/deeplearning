import os

from fastai.vision import open_image
from io import BytesIO
from flask import Flask, flash, request, redirect, url_for
from flask_cors import CORS

from MusicFinder import MusicPredict, ConvertToImage
from planeFinder import PlanePredict
import os


UPLOAD_FOLDER = './uploads'
ALLOWED_PLANE_EXTENSIONS = {'png', 'jpg', 'jpeg'}
ALLOWED_SONG_EXTENSIONS = {'mp3'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app, resources={r"*": {"origins": "*"}})


def allowed_plane_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_PLANE_EXTENSIONS


@app.route('/plane-upload', methods=['POST'])
def upload_plane_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_plane_file(file.filename):
            bitfile = request.files['file'].read()
            img = open_image(BytesIO(bitfile))
            val = PlanePredict(img)
            # filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # return redirect(url_for('uploaded_file',
            #                         filename=filename))
            return val
    return "ERROR"


def allowed_song_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_SONG_EXTENSIONS


@app.route('/song-upload', methods=['POST'])
def upload_song_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_song_file(file.filename):
            bitfile = request.files['file'].read()
            song = open('tmp.mp3','wb')
            song.write(bitfile)
            song.close()

            #Convert .mp3 to .png
            ConvertToImage('tmp.mp3')
            #Convert .png to bytes
            img = open_image('imgToPredict.png')
            print(type(img))
            # Predict
            val = MusicPredict(img)


            os.remove('imgToPredict.png')
            os.remove('tmp.mp3')

            return val
    return 'ERROR'


app.run(host='0.0.0.0')
